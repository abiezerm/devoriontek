-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-06-2021 a las 09:31:02
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `devoriontek`
--
CREATE DATABASE IF NOT EXISTS `devoriontek` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci;
USE `devoriontek`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `ID` int(11) NOT NULL,
  `NOMBRE` varchar(60) COLLATE utf8mb4_spanish_ci NOT NULL,
  `APELLIDO` varchar(60) COLLATE utf8mb4_spanish_ci NOT NULL,
  `CEDULA` varchar(15) COLLATE utf8mb4_spanish_ci NOT NULL,
  `CELULAR` varchar(15) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `TELEFONO` varchar(15) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `ESTATUS` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Truncar tablas antes de insertar `clientes`
--

TRUNCATE TABLE `clientes`;
--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ID`, `NOMBRE`, `APELLIDO`, `CEDULA`, `CELULAR`, `TELEFONO`, `ESTATUS`) VALUES
(1, 'DAISI E.', 'SILFA MENA', '112666169', '809-599-8712', '809-923-4700', 1),
(2, 'FRANCISCO JR.', 'BROWN MENA.', '00112666163', '809-599-8712', '809-923-4700', 1),
(3, 'ALTAGRACIA', 'LAPAIX NOLASCO', '10000050053', NULL, NULL, 1),
(4, 'BARTOLO', 'MEJIA CARRASCO', '10000055938', '829-755-9153', NULL, 1),
(5, 'RAUL SANTOS', 'PEÑA PEGUERRO', '10000059740', '829-250-4995', NULL, 1),
(6, 'BRIAN JESUS', 'BERNARDINO PIMENTEL', '10000061621', '809-328-0192', '809-328-0192', 1),
(7, 'MALLELIN', 'ESCARFULLER PIO', '10000080845', '829-960-5828', NULL, 1),
(8, 'MANUEL ALBERTO', 'GUERRERO DE JESUS', '00100002120', '829-491-6535', NULL, 1),
(9, 'ANEISHA', 'ORTIZ MINYETY', '23122013001', NULL, NULL, 1),
(10, 'RANSSEL', 'MINYETY', '100002120', '0', NULL, 1),
(11, 'YERSON', 'MONTERO QUEVEDO', '00100003219', '829-856-1223', '829-856-1223', 1),
(12, 'AMARILIS MARGARITA', 'DIAZ', '1000039964', '809-609-5224', NULL, 1),
(13, 'SANDRA  MARIBEL', 'CIPRIAN', '1000045573', NULL, NULL, 1),
(14, 'ELIANNY', 'RAMIREZ FLORES', '1000050664', '8098159272', NULL, 1),
(15, 'JORGE EMILIO', 'SANCHEZ G.', '1000077956', '8096801424', NULL, 1),
(16, 'LUIS FRANCISCO', 'OVIEDO MOQUETE', '1000081529', '8095959046', NULL, 1),
(17, 'GENESIS', 'QUEZADA', '10000870211', NULL, '849-654-1489', 1),
(18, 'YERLIN AMEL', 'LOPEZ SANCHEZ', '1000092112', '8096970118', NULL, 1),
(19, 'AVELINO SANTIAGO', 'CRUZ JIMENEZ', '100009760', '8092585222', NULL, 1),
(21, 'ANGEL', 'MALAQUIA SANCHEZ', '1000107282', '8293804210', NULL, 1),
(22, 'JOSE RAMON', 'BELTRE GOMERA', '1000108496', '8097033143', NULL, 1),
(23, 'MARIO ARISTIDES', 'RAMIREZ', '1000112522', '8096151185', NULL, 1),
(24, 'JUANA MATILDE DE LA ASUNCION', 'TEJEDA ROSADO', '1000113629', '8095971047', NULL, 1),
(25, 'NELSON ARIOSTO', 'PEÑA FELIZ', '1000115566', '8094247015', NULL, 1),
(26, 'DOMINGA', 'MELO CORPORAN', '100011964', NULL, NULL, 1),
(27, 'VICTOR LORENZO', 'CUEVAS NOBOA', '1000121135', NULL, NULL, 1),
(28, 'JULIO ANTONIO', 'PEÑA TORRES', '100012707', NULL, NULL, 1),
(29, 'CRISEIDA MARGARITA', 'FELIZ', '1000131241', NULL, NULL, 1),
(30, 'ESPERANZA RAMONA', 'RAMIREZ NIN', '100013234', '8095304051', NULL, 1),
(31, 'AUSTRALIA COLOMBINA', 'ANDUJAR', '1000134773', '8095335374', '8092585374', 1),
(32, 'DEYSI MARIA', 'MATOS', '1000137503', '8299949069', NULL, 1),
(33, 'MARCIAL', 'TEJEDA PAREDES', '1000153641', '8097969470', NULL, 1),
(34, 'JUAN FRANCISCO', 'DURAN LOPEZ', '100015668', '8294250231', NULL, 1),
(36, 'ANA DINSHY', 'PEREZ ROMAN', '1000159572', '8293284518', NULL, 1),
(37, 'RAMONA', 'RUIZ CASADO', '1000160992', '8296702692', '0', 1),
(38, 'we', 'wqe', 'qwe', 'wqe', 'qwe', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones`
--

DROP TABLE IF EXISTS `direcciones`;
CREATE TABLE `direcciones` (
  `ID_CLIENTE` int(10) UNSIGNED NOT NULL,
  `ID_PROVINCIA` int(10) UNSIGNED NOT NULL,
  `DIRECCION` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `ESTATUS` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Truncar tablas antes de insertar `direcciones`
--

TRUNCATE TABLE `direcciones`;
--
-- Volcado de datos para la tabla `direcciones`
--

INSERT INTO `direcciones` (`ID_CLIENTE`, `ID_PROVINCIA`, `DIRECCION`, `ESTATUS`) VALUES
(2, 2, 'ROBERTO PASTORIZO #805', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincias`
--

DROP TABLE IF EXISTS `provincias`;
CREATE TABLE `provincias` (
  `ID` int(11) NOT NULL,
  `NOMBRE` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `ESTATUS` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Truncar tablas antes de insertar `provincias`
--

TRUNCATE TABLE `provincias`;
--
-- Volcado de datos para la tabla `provincias`
--

INSERT INTO `provincias` (`ID`, `NOMBRE`, `ESTATUS`) VALUES
(1, 'Distrito Nacional', 1),
(2, 'Santo Domingo Este', 1),
(3, 'Azua', 1),
(4, 'Bahoruco', 1),
(5, 'Barahona', 1),
(6, 'Dajabón', 1),
(7, 'Duarte', 1),
(8, 'Elías Piña', 1),
(9, 'El Seibo', 1),
(10, 'Espaillat', 1),
(11, 'Hato Mayor', 1),
(12, 'Hermanas Mirabal', 1),
(13, 'Independencia', 1),
(14, 'La Altagracia', 1),
(15, 'La Romana', 1),
(16, 'La Vega', 1),
(17, 'María Trinidad Sánchez', 1),
(18, 'Monte Cristi', 1),
(19, 'Monte Plata', 1),
(20, 'Pedernales', 1),
(21, 'Peravia', 1),
(22, 'Puerto Plata', 1),
(23, 'Samaná', 1),
(24, 'San Cristóbal', 1),
(25, 'San José de Ocoa', 1),
(26, 'San Juan', 1),
(27, 'San Pedro de Macorís', 1),
(28, 'Sánchez Ramírez', 1),
(29, 'Santiago', 1),
(30, 'Santiago Rodríguez', 1),
(31, 'Santo Domingo Norte', 1),
(32, 'Valverde', 1),
(33, 'Monseñor Nouel', 1),
(34, 'Santo Domingo Oeste', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `CEDULA` (`CEDULA`);

--
-- Indices de la tabla `provincias`
--
ALTER TABLE `provincias`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `provincias`
--
ALTER TABLE `provincias`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
