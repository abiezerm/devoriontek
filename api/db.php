<?php
    /**
     * DATOS DE LA BASE DE DATOS
     */

    define ("BD_HOST",     "localhost");
    define ("BD_NAME",     "devoriontek");
    define ("BD_USER",     "root");
    define ("BD_PASSWORD", "");

    class DB{
        // Variables de conexión
        private $host       =   BD_HOST;
        private $usuario    =   BD_USER;
        private $password   =   BD_PASSWORD;
        private $base       =   BD_NAME;

        // Conectar a BD
        public function conectar(){
            $conexion_mysql =   "mysql:host=$this->host;dbname=$this->base";
            $conexionDB     =   new PDO($conexion_mysql, $this->usuario, $this->password, array(PDO::MYSQL_ATTR_LOCAL_INFILE=>true));
            $conexionDB     ->  setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            //Esta linea arregla la codificacion sino no aparecen en la salida en JSON quedan NULL
            $conexionDB     ->  exec("set names utf8");
            return $conexionDB;
        }
    }


    