<?php 
include_once('db.php');

$request = json_decode($_POST['data']);

function listado_datos($tip){

    $cn = new DB();
    $db = $cn->conectar();

    if($tip == 2){
        $sql = "SELECT * FROM  direcciones";
    }else if($tip == 3) {
        $sql = "SELECT * FROM  provincias WHERE ESTATUS = 1";   
    }else if($tip == 5){
        $sql = "SELECT
        UPPER(d.DIRECCION) AS DIRECCION,
        UPPER(CONCAT(c.NOMBRE, ' ', c.APELLIDO)) AS CLIENTE,
        UPPER(p.NOMBRE) AS PROVINCIA,
        IF(d.ESTATUS = 1,'ACTIVO', 'INACTIVO') AS ESTATUS
        FROM
        direcciones AS d
        INNER JOIN clientes AS c ON c.ID = d.ID_CLIENTE
        INNER JOIN provincias AS p ON p.ID = d.ID_PROVINCIA
        ";
    }else{
        $sql = "SELECT ID,UPPER(NOMBRE) AS NOMBRE,UPPER(APELLIDO) AS APELLIDO,CEDULA,CELULAR,TELEFONO,IF(ESTATUS = 1,'ACTIVO', 'INACTIVO') AS ESTATUS FROM  clientes";
    }
    $ejecutar   =   $db->prepare($sql);
    $ejecutar   ->  execute();
    $datos      =   $ejecutar->fetchAll(PDO::FETCH_ASSOC);
    return json_encode($datos);
}

echo listado_datos($request->TIPO);